/**
 * Costa Rica National High Technology Center
 * Advanced Computing Laboratory
 * Costa Rica HPC School 2020 
 * Instructor: Esteban Meneses, PhD
 * MPI ping-pong program.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#define FLAG 7

// Main routine
int main (int argc, char *argv[]){
	int rank, size;
	int number;
	double start_time, end_time;

	// initialize MPI
	MPI_Init(&argc, &argv);	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	
	MPI_Comm_size(MPI_COMM_WORLD, &size);	

	// sending and receiving a message
	if(rank == 0){
		start_time = MPI_Wtime();

		// TO DO: send and receive message
		
		end_time = MPI_Wtime();
		printf("Elapsed time: %f seconds\n",end_time-start_time);
	} else {

		// TO DO: receive and send message

	}

	// finalize MPI
	MPI_Finalize();
	return 0;
}
