/**
 * Costa Rica National High Technology Center
 * Advanced Computing Laboratory
 * Costa Rica HPC School 2020 
 * Instructor: Esteban Meneses, PhD
 * MPI Smith-Waterman.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define TAG 7
#define DIMENSION_X 0
#define DIMENSION_Y 1
#define BLOCK_SIZE 1000
#define LIMIT_SIZE 100000

void loadSequence(char *fileName, char *buffer, int *length);

// Main function
int main(int argc, char* argv[]){
	int myRank;									// Rank of process
	int p;										// Number of processes
	int previous;								// Previous process in the ring
	int next;									// Next process in the ring
	int tag = TAG;								// Tag for message
	int column;
	int row;
	int j;
	double value,top,left,topleft;
	char symbol;								// the base of y currently being analyzed by the thread
	MPI_Status status;
	int rounds;
	double values[BLOCK_SIZE];
	char *fileX, *fileY;
	char *x, *y, *inter;
	int lengthX, lengthY;
	time_t initialTime, totalTime;

	// checking for command arguments
	if(argc < 3){
		printf("ERROR, usage: %s <file sequence x> <file sequence y>\n",argv[0]);
		exit(-1);
	}

	// storing name of files storing sequences x and y
	fileX = argv[1];
	fileY = argv[2];

	// initializing MPI structures
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	next = (myRank + 1) % p;
	previous = (myRank - 1 + p) % p;	

	// taking initial time
	if(myRank == 0)
		initialTime = MPI_Wtime();

	// initializing arrays to store
	x = (char *) malloc(LIMIT_SIZE);
	y = (char *) malloc(LIMIT_SIZE);
	inter = (char *) malloc(LIMIT_SIZE);

	// loading sequences from the file (only processor 0 performs I/O)
	if(myRank == 0){
		loadSequence(fileX,x,&lengthX);
		loadSequence(fileY,y,&lengthY);
	}

	// broadcasting the sequences x and y
	MPI_Bcast(&lengthX,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&lengthY,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(x,lengthX,MPI_CHAR,0,MPI_COMM_WORLD);
	MPI_Bcast(y,lengthY,MPI_CHAR,0,MPI_COMM_WORLD);

	// initial values for matrix values
	value = top = left = topleft = 0.0;

	// running the algorithm
	column = myRank;
	
	// going through all the y axis of the matrix
	while(column < lengthY){

		// getting the base in y for this thread
		symbol = y[column];
		row = 0;
		left = topleft = top = 0.0;
		
		// going through all the rows of the matrix (i.e. analyzing all the bases in sequence x)
		while(row < lengthX-1){

			// getting the chunk of values coming from previous processor
			rounds = (row+BLOCK_SIZE > lengthX)? lengthX - row : BLOCK_SIZE;
			if(column != 0){

				// TO DO: receive values				
			}			

			// running the algorithm for that chunk
			for(j = 0; j < rounds; j++,row++){
				if(column != 0){
					left = values[j];
				}

				// applying Smith-Waterman
				if(x[row] == symbol)
					value = topleft + 1;
				else
					value = (top>left)? top : left;
				
				// storing result in the same buffer
				values[j] = value;	

				// updating variables for next iteration
				top = value;
				topleft = left;

			}

			// sending chunk to next processor
			if(column != lengthY - 1){

				// TO DO: send values
			} 

		}

		// printing the result
		if(column == (lengthY - 1)){
			printf("Length of common subsequence: %.0f\n",value);
		}

		// updating column value
		column = column + p;
	}

	// synchronizing threads
	MPI_Barrier(MPI_COMM_WORLD);

	// taking final time
	if(myRank == 0){
		totalTime = MPI_Wtime() - initialTime;
		printf("Total time: %d s\n",totalTime);
	}

	// free memory
	free(x);
	free(y);
	free(inter);

	// finalizing MPI structures
	MPI_Finalize();

}

// Function to load sequences stored in files
void loadSequence(char *fileName, char *buffer, int *length){
	FILE *stream;	
	int i;

	// opening the file
	stream = fopen(fileName, "r");
	
	// reading the whole content
	for(i = 0; !feof(stream); i++){
		if(i > LIMIT_SIZE)
			break;
		buffer[i] = fgetc(stream);
	}

	printf("%d bytes read from %s\n", i, fileName);
	
	// setting the number of bytes read
	(*length) = i;

	// closing the file
	fclose(stream);
	
}


