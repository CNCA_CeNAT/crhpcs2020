/*Sequential Solution to Steady-State Heat Problem*/

#include <omp.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 25
#define NUM_STEPS 6000 

/*writeFile: this function writes simulation results into a file. A file is created for each iteration that's passed 
 * to the function as a parameter. It also takes the triple pointer to the simulation data*/
void writeFile(int iteration, double*** data){
	char filename[50];
	char itr[12];
	sprintf(itr, "%d", iteration);


	strcpy(filename, "heat_");
	strcat(filename, itr);
	strcat(filename, ".txt");
	//printf("Filename is %s\n", filename);

	FILE *fp;
	fp = fopen(filename, "w");


	fprintf(fp, "x,y,z,T\n");
	for(int i=0; i<N; i++){
		for(int j=0;j<N; j++){
			for(int k=0; k<N; k++){
				fprintf(fp,"%d,%d,%d,%f\n", i,j,k,data[i][j][k]);
			}
		}
	}

	fclose(fp);

}


int main(int argc, char *argv[]){

	int i,j,k,nsteps; 
	double mean; /*Average boundary value*/

	double ***arrayOld; //Variable that will hold the data of the past iteration
	double ***arrayNew; //Variable were newly computed data will be stored


        /*********Allocating memory for arrayOld and arrayNew***********************/
	
	arrayOld = (double***)malloc(N*sizeof(double**));
	arrayNew = (double***)malloc(N*sizeof(double**));
	if(arrayOld== NULL){
		fprintf(stderr, "Out of memory");
		exit(0);
	}
	for(i=0; i<N;i++){
		arrayOld[i] = (double**)malloc(N*sizeof(double*));
		arrayNew[i] = (double**)malloc(N*sizeof(double*));
		if(arrayOld[i]==NULL){
			fprintf(stderr, "Out of memory");
			exit(0);
		}
		
		for(int j=0;j<N;j++){
			arrayOld[i][j] = (double*)malloc(N*sizeof(double));
			arrayNew[i][j] = (double*)malloc(N*sizeof(double));
			if(arrayOld[i][j]==NULL){
				fprintf(stderr,"Out of memory");
				exit(0);
			}
		}
	}
	/************************************************************************/

	/******************* Data initialization*********************************/
	


	/*Set boundary values and compute mean boundary values*/
	mean = 0.0;
	
	#pragma omp parallel for reduction(+:mean) private(i,j)
	for(i=0; i<N; i++){
		for(j=0;j<N;j++){
			arrayOld[i][j][0] = 100.0;
			mean += arrayOld[i][j][0];
		}
	}

	#pragma omp parallel for reduction(+:mean) private(i,j)
	for(i=0; i<N; i++){
		for(j=0;j<N;j++){
			arrayOld[i][j][N-1] = 100.0;
			mean += arrayOld[i][j][N-1];
		}
	}


	#pragma omp parallel for reduction(+:mean) private(j,k)
	for(j=0; j<N; j++){
		for(k=0;k<N;k++){
			arrayOld[0][j][k] = 100.0;
			mean += arrayOld[0][j][k];
		}
	}
	

	#pragma omp parallel for reduction(+:mean) private(j,k)
	for(j=0; j<N; j++){
		for(k=0;k<N;k++){
			arrayOld[N-1][j][k] = 100.0;
			mean += arrayOld[N-1][j][k];
		}
	}

	#pragma omp parallel for reduction (+:mean) private(i,k)
	for(i=0; i<N; i++){
		for(k=0;k<N;k++){
			arrayOld[i][0][k] = 100.0;
			mean += arrayOld[i][0][k];
			
		}
	}

	
	#pragma omp parallel for reduction (+:mean) private(i,k)
	for(i=0; i<N; i++){
		for(k=0;k<N;k++){
			arrayOld[i][N-1][k] = 0.0;
			mean += arrayOld[i][N-1][k];
			
		}
	}


	mean /= (6.0 * (N*N));

	/*Initialize interior values*/
	#pragma omp parallel for shared(arrayOld, mean) private (i,j,k) 	
	for(i=1; i<N-1; i++){
		for(j=1; j<N-1; j++){
			for(k=1; k<N-1;k++){
				arrayOld[i][j][k] = mean;
			}
		}
	}

	/************************************************************************/




	/********************Compute steady-state solution**********************/


	double tdata = omp_get_wtime(); //Time measurement 

	for(nsteps=0; nsteps < NUM_STEPS; nsteps++){
		//To avoid huge IO overhead we print data every 100 iteration steps
		/*if(nsteps % 100 == 0){
			writeFile(nsteps, arrayOld);
		}*/


		#pragma omp parallel shared(arrayNew, arrayOld) private(i,j,k)
		{
			
			#pragma omp for 
			for(i=1; i<N-1; i++){
				for(j=1; j<N-1; j++){
					for(k=1;k<N-1;k++){
						arrayNew[i][j][k] = (arrayOld[i-1][j][k] + arrayOld[i+1][j][k] + arrayOld[i][j-1][k] + arrayOld[i][j+1][k] + arrayOld[i][j][k-1] + arrayOld[i][j][k+1])/6.0;
					}
				}
			}
			#pragma omp for 
			for(i=1; i<N-1; i++){
				for(j=1; j<N-1; j++){
					for(k=1; k<N-1; k++){
						arrayOld[i][j][k] = arrayNew[i][j][k];
					}
				}
			}
	       
		}
	}

	tdata = omp_get_wtime()-tdata;
	/*********************************************************************/
	

	printf("Execution time was %f secs\n", tdata);
	return 0;
}

