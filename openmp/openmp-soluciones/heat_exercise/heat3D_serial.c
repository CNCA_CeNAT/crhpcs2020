/***********************************************************/
/*
This program computes the steady state heat equation over a
cubic region.

The physical region and the boundary conditions are shown in 
this diagram:

                   W = 100
             +------------------+
             |                  |
    W = 100  |                  | W = 100
             |                  |
             +------------------+
                   W = 0

A 3D 6-neighbor stencil pattern is used to compute the values
of the solution in the interior of the cube:

W[Central]  <=  (1/6) * ( W[North] + W[South] + W[East] + W[West] + W[Front] + W[Back])



*/
/**********************************************************/





/*Sequential Solution to Steady-State Heat Problem*/







#include <omp.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 25
#define NUM_STEPS 6000 


void writeFile(int iteration, double*** data){
	char filename[50];
	char itr[12];
	sprintf(itr, "%d", iteration);


	strcpy(filename, "heat_");
	strcat(filename, itr);
	strcat(filename, ".txt");
	//printf("Filename is %s\n", filename);

	FILE *fp;
	fp = fopen(filename, "w");


	fprintf(fp, "x,y,z,T\n");
	for(int i=0; i<N; i++){
		for(int j=0;j<N; j++){
			for(int k=0; k<N; k++){
				fprintf(fp,"%d,%d,%d,%f\n", i,j,k,data[i][j][k]);
			}
		}
	}

	fclose(fp);

}


int main(int argc, char *argv[]){

	int i,j,k,nsteps; 
	double mean; /*Average boundary value*/

	double ***arrayOld;
	double ***arrayNew;

	arrayOld = (double***)malloc(N*sizeof(double**));
	arrayNew = (double***)malloc(N*sizeof(double**));
	if(arrayOld== NULL){
		fprintf(stderr, "Out of memory");
		exit(0);
	}
	for(i=0; i<N;i++){
		arrayOld[i] = (double**)malloc(N*sizeof(double*));
		arrayNew[i] = (double**)malloc(N*sizeof(double*));
		if(arrayOld[i]==NULL){
			fprintf(stderr, "Out of memory");
			exit(0);
		}
		
		for(int j=0;j<N;j++){
			arrayOld[i][j] = (double*)malloc(N*sizeof(double));
			arrayNew[i][j] = (double*)malloc(N*sizeof(double));
			if(arrayOld[i][j]==NULL){
				fprintf(stderr,"Out of memory");
				exit(0);
			}
		}
	}



	/*Set boundary values and compute mean boundary values*/
	mean = 0.0;

	for(i=0; i<N; i++){
		for(j=0;j<N;j++){
			arrayOld[i][j][0] = 100.0;
			mean += arrayOld[i][j][0];
		}
	}

	
	for(i=0; i<N; i++){
		for(j=0;j<N;j++){
			arrayOld[i][j][N-1] = 100.0;
			mean += arrayOld[i][j][N-1];
		}
	}


	for(j=0; j<N; j++){
		for(k=0;k<N;k++){
			arrayOld[0][j][k] = 100.0;
			mean += arrayOld[0][j][k];
		}
	}
	

	for(j=0; j<N; j++){
		for(k=0;k<N;k++){
			arrayOld[N-1][j][k] = 100.0;
			mean += arrayOld[N-1][j][k];
		}
	}


	for(i=0; i<N; i++){
		for(k=0;k<N;k++){
			arrayOld[i][0][k] = 100.0;
			mean += arrayOld[i][0][k];
			
		}
	}

	
	for(i=0; i<N; i++){
		for(k=0;k<N;k++){
			arrayOld[i][N-1][k] = 0.0;
			mean += arrayOld[i][N-1][k];
			
		}
	}


	mean /= (6.0 * (N*N));

	/*Initialize interior values*/
	for(i=1; i<N-1; i++){
		for(j=1; j<N-1; j++){
			for(k=1; k<N-1;k++){
				arrayOld[i][j][k] = mean;
			}
		}
	}

	
	double tdata = omp_get_wtime();
	/*Compute steady-state solution*/
	for(nsteps=0; nsteps < NUM_STEPS; nsteps++){
		/*if(nsteps % 100 == 0){
			writeFile(nsteps, arrayOld);
		}*/
		for(i=1; i<N-1; i++){
			for(j=1; j<N-1; j++){
				for(k=1;k<N-1;k++){
					arrayNew[i][j][k] = (arrayOld[i-1][j][k] + arrayOld[i+1][j][k] + arrayOld[i][j-1][k] + arrayOld[i][j+1][k] + arrayOld[i][j][k-1] + arrayOld[i][j][k+1])/6.0;
				}
			}
		}
		for(i=1; i<N-1; i++){
			for(j=1; j<N-1; j++){
				for(k=1; k<N-1; k++){
					arrayOld[i][j][k] = arrayNew[i][j][k];
				}
			}
		}
	}
	 
	tdata = omp_get_wtime()-tdata;

	printf("Execution time was %f secs\n", tdata);
	
	return 0;
}

