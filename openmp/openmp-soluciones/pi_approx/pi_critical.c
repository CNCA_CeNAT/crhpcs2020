#include <omp.h>
#include <stdio.h>


#define NUM_THREADS 64

static long num_steps = 100000000;
double step;


int main(){
	int nthreads;
	double pi = 0.0;

	step = 1.0/(double) num_steps;

	omp_set_num_threads(NUM_THREADS);

	double tdata = omp_get_wtime();
	#pragma omp parallel
	{
		int i, thread_id, num_threads;
		double x, sum;
	
		thread_id = omp_get_thread_num();
		num_threads = omp_get_num_threads();

		if(thread_id == 0){
			nthreads = num_threads;
		}
		for(i=0, sum = 0.0; i<num_steps;i=i+num_threads){
			x = (i+0.5)*step;
			sum = sum + 4.0/(1.0 + x*x);	
		}

		#pragma omp critical
		pi += sum*step;
	}
	
	tdata = omp_get_wtime()-tdata;

	printf("pi = %.*f in %f secs\n", 12, pi, tdata);

}
